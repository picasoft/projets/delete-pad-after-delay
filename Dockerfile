FROM perl:5-buster

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -y install build-essential libssl-dev libio-socket-ssl-perl locales

# Strange enough, the Perl image comes with only POSIX locale installed
# This makes fetching pad with UTF-8 characters very tedious
RUN locale-gen en_US.UTF-8 && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    echo 'LANG="en_US.UTF-8"'>/etc/default/locale && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8

ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8  

RUN cpan Carton

RUN mkdir -p /opt/delete_after_delay
COPY ./delete_after_delay.pl ./cpanfile.snapshot ./cpanfile /opt/delete_after_delay/

WORKDIR /opt/delete_after_delay
RUN carton install
COPY ./docker/init /

ENV READ_FILE_API_KEY_RETRIES 10
ENV READ_FILE_API_KEY_DELAY_BETWEEN_RETRIES_SECONDS 30
ENV QUERY_API_RETRIES 10
ENV QUERY_API_DELAY_BETWEEN_RETRIES_SECONDS 30
ENV DELAY_BETWEEN_CHECK 604800
ENTRYPOINT ["/init"]

CMD ["carton", "exec", "/opt/delete_after_delay/delete_after_delay.pl", "--", "--verbose", "--daemon", "--instance", "instanceD"]
