Fork du dépôt : https://framagit.org/framasoft/Etherpad/pad_delete_after_delay
Adapté pour Docker.

# PAD\_DELETE\_AFTER\_DELAY

At [Framasoft](https://framasoft.org), we host a lot of heavily-used [Etherpad](http://etherpad.org) instances.

We wanted to have time-limited pads, so [Luc Didry](https://luc.frama.io) developped a [plugin](https://framagit.org/luc/ep_delete_after_delay) which deletes pads after a configured delay.

But we have so many pads (more than 30k on some instances) that the plugin fails to work correctly: it causes a really huge load on the etherpad processus at start and stuck it.

So we developped an external script to delete the expired pads without loading the etherpad processus.

`delete_after_delay` asks an etherpad instance for the list of its pads, then check (and delete if expired) all of them.

# Usage

Build the image, see [README](./docker/README.md).

# License

GNU General Public License, version 3. Check the [LICENSE file](LICENSE).
